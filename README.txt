About Elegant Blue Theme
====================
Elegant Blue is a fixed width (940px) theme. The theme is not dependent
on any core theme. Its very light weight for fast loading with modern
look.

- Simple and clean design
- Fixed width (940px)
- Drupal standards compliant
- Custom front-page with 1 Welcome text block and 3 column block with
  theme settings available.
- Implementation of a JS Slideshow
- Multi-level drop-down menus
- Use of Google Web Fonts
- No sidebar blocks on frontpage

Elegant Blue Theme Settings
====================
User need to put theme at sites/all/themes.

Following configuration option available at theme settings page.
(appereance -> Settings for Elegant Blue theme)

[1] Breadcrumb Display option.
[2] Front Page Slideshow option for the Slide Description.
[3] Social Icon URL option.(User can set url for the social icon links)
[4] Title & Description for the Front page Welcome Text.
[5] Have options of title and descrition for the three column block
    displaying on front page.
[6] Two bottom block regions
[7] Footer Options.

For Drop-Down-Menu:
=====================
To Enable drop-down menu, Select "Show as expanded " at setting page of
menu items.
 
Drupal compatibility:
=====================
This theme is compatible with Drupal 7.x.x

Design & Developed by
============
http://www.about.me/ankithinglajia
